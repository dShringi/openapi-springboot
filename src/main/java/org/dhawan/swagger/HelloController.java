package org.dhawan.swagger;

import io.swagger.v3.core.util.Json;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {

    @RequestMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }

    @RequestMapping(path = "/{input}", method = RequestMethod.POST, produces = "application/json")
    public Input postRequest(@PathVariable String input) {
        return new Input(input);
    }

}