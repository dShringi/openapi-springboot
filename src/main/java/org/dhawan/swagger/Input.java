package org.dhawan.swagger;

public class Input {

    public Input() {}
    public Input(String input) {
        this.input = input;
    }

    private String input;

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    @Override
    public String toString() {
        return "{ \"input\":\"" + input + "\"}";
    }
}
